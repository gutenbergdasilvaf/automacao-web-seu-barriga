package testes;

import static org.junit.Assert.assertEquals;

import java.util.Locale;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.github.javafaker.service.FakeValuesService;
import com.github.javafaker.service.RandomService;

public class CadastroTeste {
	//VARIAVÉIS DE CONFIGURAÇÃO
	public static final String PATH_DRIVER = ".\\src\\test\\resources\\drivers\\chromedriver.exe";	
	public static final String URL = "http://seubarriga.wcaquino.me/login";
	public static WebDriver DRIVER;
	public static WebDriverWait WAIT;
	
	//DADOS DE TESTE
	public static String NOME_VALIDO;
	public static String EMAIL_VALIDO;
	public static String SENHA_VALIDA;
	
	public static String NOME_VAZIO= "";
	public static String EMAIL_VAZIO = "";
	public static String SENHA_VAZIA = "";
	
	//MENSAGENS ESPERADAS	
	public static final String USUARIO_CRIADO_COM_SUCESSO_MENSAGEM_ESPERADA = "Usuário inserido com sucesso";
	public static final String NOME_OBRIGATORIO_MENSAGEM_ESPERADA = "Nome é um campo obrigatório";
	public static final String EMAIL_OBRIGATORIO_MENSAGEM_ESPERADA = "Email é um campo obrigatório";
	public static final String SENHA_OBRIGATORIA_MENSAGEM_ESPERADA = "Senha é um campo obrigatório";

	
	
	@BeforeClass
	public static void setup() {	
		
		// GERANDO DADOS DE TESTE FAKE
	    FakeValuesService fakeValuesService = new FakeValuesService(new Locale("en-GB"), new RandomService());
	    NOME_VALIDO = fakeValuesService.bothify("????##");
	    EMAIL_VALIDO = fakeValuesService.bothify("????##@gmail.com");
	    SENHA_VALIDA = fakeValuesService.bothify("????##");
	    
		//CONFIGURANDO DRIVER E WAIT
		System.setProperty("webdriver.chrome.driver", PATH_DRIVER);	  
	    DRIVER = new ChromeDriver();
	    WAIT = new WebDriverWait(DRIVER, 3);
	    
	    //ABRINDO NAVEGADOR E ACESSANDO SISTEMA
	    DRIVER.get(URL);
	    
	    //MAXIMIZANDO TELA
	    DRIVER.manage().window().maximize();  	    
	    
	}
	
	@AfterClass
	public static void tearDown() {		
		DRIVER.quit();
	}
	
	@Test
	public void teste_CadastroUsuarioComCamposValidos() {		
		
		//ACESSANDO PÁGINA DE CADASTRO
		WebElement MENU_NOVO_USUARIO = DRIVER.findElement(By.xpath("//*[@id=\"bs-example-navbar-collapse-1\"]/ul/li[2]/a"));
		MENU_NOVO_USUARIO.click();
		
		//ESPERANDO ELEMENTOS FICAREM VISIVEIS
		WAIT.until(ExpectedConditions.visibilityOfElementLocated(By.id("nome")));
		WAIT.until(ExpectedConditions.visibilityOfElementLocated(By.id("email")));
		WAIT.until(ExpectedConditions.visibilityOfElementLocated(By.id("senha")));
		
		//PROCURANDO ELEMENTOS DA PÁGINA		
		WebElement CAMPO_NOME = DRIVER.findElement(By.id("nome"));
		WebElement CAMPO_EMAIL = DRIVER.findElement(By.id("email"));
		WebElement CAMPO_SENHA = DRIVER.findElement(By.id("senha"));
		
		//PREENCHENDO CAMPOS
		CAMPO_NOME.sendKeys(NOME_VALIDO);
		CAMPO_SENHA.sendKeys(SENHA_VALIDA);
		CAMPO_EMAIL.sendKeys(EMAIL_VALIDO);
		
		//CLICANDO EM CADASTRAR
		WebElement BOTAO_CADASTRAR = DRIVER.findElement(By.xpath("/html/body/div[2]/form/input"));
		BOTAO_CADASTRAR.click();
		
		//VALIDAÇÕES
		WebElement USUARIO_CRIADO_COM_SUCESSO_MENSAGEM_ATUAL = DRIVER.findElement(By.xpath("/html/body/div[1]"));
		assertEquals(USUARIO_CRIADO_COM_SUCESSO_MENSAGEM_ATUAL.getText(), USUARIO_CRIADO_COM_SUCESSO_MENSAGEM_ESPERADA);	
				
	}
	
	@Test
	public void teste_TentarCadastrarUsuarioComCamposVazios() {
		
		//ACESSANDO PAGINA DE CADASTRO
		WebElement MENU_NOVO_USUARIO = DRIVER.findElement(By.xpath("//*[@id=\"bs-example-navbar-collapse-1\"]/ul/li[2]/a"));
		MENU_NOVO_USUARIO.click();
				
		//ESPERANDO ELEMENTOS FICAREM VISIVEIS
		WAIT.until(ExpectedConditions.visibilityOfElementLocated(By.id("nome")));
		WAIT.until(ExpectedConditions.visibilityOfElementLocated(By.id("email")));
		WAIT.until(ExpectedConditions.visibilityOfElementLocated(By.id("senha")));
				
		//PROCURANDO ELEMENTOS DA PÁGINA			
		WebElement CAMPO_NOME = DRIVER.findElement(By.id("nome"));
		WebElement CAMPO_EMAIL = DRIVER.findElement(By.id("email"));
		WebElement CAMPO_SENHA = DRIVER.findElement(By.id("senha"));				
				
		//PREENCHENDO CAMPOS
		CAMPO_NOME.sendKeys(NOME_VAZIO);
		CAMPO_SENHA.sendKeys(SENHA_VAZIA);
		CAMPO_EMAIL.sendKeys(EMAIL_VAZIO);
				
		//CLICANDO EM CADASTRAR
		WebElement BOTAO_CADASTRAR = DRIVER.findElement(By.xpath("/html/body/div[2]/form/input"));
		BOTAO_CADASTRAR.click();				
				
		//VALIDAÇÕES
		WebElement NOME_OBRIGATORIO_MENSAGEM_ATUAL = DRIVER.findElement(By.xpath("/html/body/div[1]"));
		WebElement EMAIL_OBRIGATORIO_MENSAGEM_ATUAL = DRIVER.findElement(By.xpath("/html/body/div[2]"));
		WebElement SENHA_OBRIGATORIA_MENSAGEM_ATUAL = DRIVER.findElement(By.xpath("/html/body/div[3]"));
		
		assertEquals(NOME_OBRIGATORIO_MENSAGEM_ATUAL.getText(), NOME_OBRIGATORIO_MENSAGEM_ESPERADA);	
		assertEquals(EMAIL_OBRIGATORIO_MENSAGEM_ATUAL.getText(), EMAIL_OBRIGATORIO_MENSAGEM_ESPERADA);	
		assertEquals(SENHA_OBRIGATORIA_MENSAGEM_ATUAL.getText(), SENHA_OBRIGATORIA_MENSAGEM_ESPERADA);	
					
	}	
	

}

